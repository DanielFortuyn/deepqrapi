'use strict';
const axios = require('axios');
const QR = require("../../../app/qr");
const Scan = require("../../../app/scan");
const Output = require('../../../app/output');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    cqr: async ctx => {
        console.log(ctx.request.body)
        return true;
    },
    createqr: async ctx => {
        const qrlib = new QR();
        const output = new Output();

        const result = await strapi.services.qr.create(ctx.request.body);
        const qrcode = await qrlib.buildQr(result);

        ctx = await output.output(ctx, result, qrcode);
        return;
    },

    scan: async ctx => {
        // const id = ctx.params.id;
        const scans = new Scan();
        const { uuid } = ctx.params;
        const entity = await strapi.services.qr.findOne({ uuid });
        if (entity) {
            entity.uses++
            strapi.services.qr.update(entity.id, entity);
            try {
                await strapi.services.scan.create({ qr: parseInt(entity.id), ctx: JSON.stringify(ctx.request) });
            } catch (error) {
                console.log(error);
            }
            ctx.redirect(scans.buildUrl(entity));
        }
        return;
    },
};