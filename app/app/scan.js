function Scan() {
    this.buildUrl = function(qrObject) {
        return qrObject.url + "?" + this.buildQuery(qrObject);
    }
    this.buildQuery = function (qrObject) {
        const obj = qrObject.data;
        var str = [];
        console.log(qrObject);
        if(qrObject.encryptUrl) {
            console.log("Not implemented")
            str.push('encryption=notimplemented');
        } else {
            for (var p in obj)
                if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        }
        return str.join("&");
    }
}

module.exports = Scan;