const QRCode = require('qrcode');

function Qr() {
    this.buildUrl = function(qrObject) {
        return strapi.config.get('app.base', 'https://q.frtn.nl/') + "s/" + qrObject.uuid;
    }
    this.buildQr = async function(qrObject) {
        return await QRCode.toDataURL(this.buildUrl(qrObject));
    }
}

module.exports = Qr;