function Output() {
    this.json = async (ctx, result, qr) => {
        ctx.body = {
            entity: {
                ...result
            },
            qr: {
                dataUrl: qr,
            }
        }
        return ctx;
    }
    this.image = async (ctx, result, qrcode) => {
        return this.png(ctx, result, qrcode)
    }

    this.png = async (ctx, result, qrcode) => {
        const data = qrcode.split(',')[1];
        var buf = Buffer.from(data, 'base64');
        ctx.response.set("content-type", 'image/png')
        ctx.body = buf;
        return ctx;
    }

    this.output = async (ctx, result, qrcode) => {
        let output = 'json';
        if (ctx.request.body.output) {
            output = ctx.request.body.output.type;

            if (output == 'json') {
                ctx = await this.json(ctx, result, qrcode);
            } else if (output == 'png' || output == 'image') {
                ctx = await this.png(ctx, result, qrcode);
            } else {
                ctx.body ='output not supported';
            }
        }
        return ctx;
    }
}

module.exports = Output;