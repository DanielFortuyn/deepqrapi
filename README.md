# DeepQR

DeeQR is a small project aimed at easily generating redirect QRs and tracking the use of them. It is build in NodeJS and uses strapi as an underlying framework. This is an initial version. I might add more functionality to it later on. If you want to use this please feel free to work and or send me a message to use a deployed version of this tool.

You can read a bit more on the app: https://frtn.nl/deepqr/.

## Sample request

```
{
    "url": "https://frtn.nl/",
    "method": "GET",
    "encryptUrl": false,
    "data": {
        "type": "example"
    },
    "user": 1,
    "output": {
        "type": "json",
        "size": 200
    }
}
```

## Sample response

```
{
    "entity": {
        "id": 12,
        "data": {
            "type": "example"
        },
        "method": "GET",
        "url": "https://frtn.nl/",
        "uses": null,
        "page": null,
        "encryptUrl": false,
        "uuid": "22ab3975-909b-433a-a347-20f2a66bd22c",
        "type": null,
        "created_by": null,
        "updated_by": null,
        "created_at": "2021-04-11T19:12:13.276Z",
        "updated_at": "2021-04-11T19:12:13.276Z"
    },
    "qr": {
        "dataUrl": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKQAAACkCAYAAAAZtYVBAAAAAklEQVR4AewaftIAAAZASURBVO3BQY4cSRLAQDLQ//8yV0c/JZCoam1o4Gb2B2td4rDWRQ5rXeSw1kUOa13ksNZFDmtd5LDWRQ5rXeSw1kUOa13ksNZFDmtd5LDWRQ5rXeSw1kV++JDK31QxqUwVk8qTiknlScUnVKaKSWWqmFSmiicqf1PFJw5rXeSw1kUOa13khy+r+CaVN1SmikllUnlS8UTljYpJ5W+q+CaVbzqsdZHDWhc5rHWRH36ZyhsVb1RMKpPKk4pPVHyi4onKVDGpTBVvqLxR8ZsOa13ksNZFDmtd5Id/nMpU8UTlDZWpYlJ5o+KNiicV/yWHtS5yWOsih7Uu8sM/rmJSmSqeqHyiYlJ5ojJVTCpPKv7LDmtd5LDWRQ5rXeSHX1bxN1VMKlPFE5Wp4hMVk8obFU9Upoo3Km5yWOsih7UucljrIj98mcrfpDJVvKEyVUwqU8WkMlVMKlPFpDJVTCpTxSdUbnZY6yKHtS5yWOsiP3yo4mYVn6h4UjGpTBWTyhsVk8obFf+Sw1oXOax1kcNaF/nhQypTxaTypGJSeaPiicpU8U0qT1TeUPkmlaniicpUMak8qfjEYa2LHNa6yGGti/zwyyreqPiEyhOVqeKJylQxVTxReVIxqUwVk8pU8YbKVPFEZaqYVL7psNZFDmtd5LDWRX74UMWkMlVMKv8lKlPFVPFGxSdUpopJZVJ5UvE3Hda6yGGtixzWusgPH1KZKiaVqWJSeVIxqTyp+ITKGxWfUJkqJpU3Kt6omFSeqPymw1oXOax1kcNaF/nhy1SmikllqphUJpWp4onKk4onFU9UpopJ5Y2KNyomlScVT1TeqPhNh7UucljrIoe1LvLDl1VMKm9UPFGZKqaKSeWJypOKqWJS+SaVqeJJxRsVb6g8qfimw1oXOax1kcNaF/nhQxVvVEwqU8WkMlVMKk8qJpWp4g2VJxVPVL5JZaqYVD5R8Tcd1rrIYa2LHNa6yA9fpvJE5YnKVDGpTBWTyqTyhsqTiknlEypvqEwVk8obFU9UporfdFjrIoe1LnJY6yI//LKKSeUNlScqb1R8QuUNlaliUnmi8kTljYpJZar4fzqsdZHDWhc5rHUR+4NfpPJGxROVqWJSmSomlaniDZUnFd+k8kbFpDJVTCpPKiaVJxWfOKx1kcNaFzmsdZEfvkxlqphUpopJ5RMVk8pU8YbKGypPKiaVqeJJxaTypGJSeVIxqTyp+KbDWhc5rHWRw1oXsT/4RSpvVDxR+UTFE5VvqphUnlS8ofJGxaQyVTxReVLxicNaFzmsdZHDWhf54ctUpoonKk9UnlS8oTJVTBWfUJlU3lB5UvFGxZOKSeX/6bDWRQ5rXeSw1kXsDz6g8qRiUpkqPqEyVUwqn6iYVKaKJypPKp6ofFPFE5UnFZPKVPGJw1oXOax1kcNaF/nhQxW/SeVJxTdVTCpTxaQyVUwVT1Q+UfFEZVJ5o2JS+U2HtS5yWOsih7Uu8sOXqUwVU8UTlaniiconKm5SMalMFZPKVDFVPFGZKt6o+KbDWhc5rHWRw1oX+eFDKlPFE5UnFZPKJyqeqEwVT1SeqDyp+E0qn1CZKqaKSWWq+MRhrYsc1rrIYa2L2B/8w1SeVDxR+UTFpPJGxRsqTyreUHlSMalMFd90WOsih7UucljrIj98SOVvqnhSMalMFVPFpDJVfKJiUplUfpPKVPGk4v/psNZFDmtd5LDWRX74sopvUvlExaTypGJSeVIxVXxTxaTyRsUbKk8qftNhrYsc1rrIYa2L/PDLVN6oeKNiUvmmiknlicpU8aRiUplU3lD5RMWk8kRlqvjEYa2LHNa6yGGti/zwj1OZKiaVJxWTylQxVTypeFLxiYpJ5UnFpDJVPKn4mw5rXeSw1kUOa13kh/+4ijcqJpWp4onKk4pJ5Q2VqeKJyhsqU8WTim86rHWRw1oXOax1kR9+WcVvqphU3qh4Q+U3VbyhMlW8ofIJlaniE4e1LnJY6yKHtS7yw5ep/E0qTyqeqDypmFSmikllqphUpopJ5UnFVPFGxaTyRGWqmCq+6bDWRQ5rXeSw1kXsD9a6xGGtixzWushhrYsc1rrIYa2LHNa6yGGtixzWushhrYsc1rrIYa2LHNa6yGGtixzWushhrYv8D90dFmgfq6akAAAAAElFTkSuQmCC"
    }
    
```
# Result

<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKQAAACkCAYAAAAZtYVBAAAAAklEQVR4AewaftIAAAZASURBVO3BQY4cSRLAQDLQ//8yV0c/JZCoam1o4Gb2B2td4rDWRQ5rXeSw1kUOa13ksNZFDmtd5LDWRQ5rXeSw1kUOa13ksNZFDmtd5LDWRQ5rXeSw1kV++JDK31QxqUwVk8qTiknlScUnVKaKSWWqmFSmiicqf1PFJw5rXeSw1kUOa13khy+r+CaVN1SmikllUnlS8UTljYpJ5W+q+CaVbzqsdZHDWhc5rHWRH36ZyhsVb1RMKpPKk4pPVHyi4onKVDGpTBVvqLxR8ZsOa13ksNZFDmtd5Id/nMpU8UTlDZWpYlJ5o+KNiicV/yWHtS5yWOsih7Uu8sM/rmJSmSqeqHyiYlJ5ojJVTCpPKv7LDmtd5LDWRQ5rXeSHX1bxN1VMKlPFE5Wp4hMVk8obFU9Upoo3Km5yWOsih7UucljrIj98mcrfpDJVvKEyVUwqU8WkMlVMKlPFpDJVTCpTxSdUbnZY6yKHtS5yWOsiP3yo4mYVn6h4UjGpTBWTyhsVk8obFf+Sw1oXOax1kcNaF/nhQypTxaTypGJSeaPiicpU8U0qT1TeUPkmlaniicpUMak8qfjEYa2LHNa6yGGti/zwyyreqPiEyhOVqeKJylQxVTxReVIxqUwVk8pU8YbKVPFEZaqYVL7psNZFDmtd5LDWRX74UMWkMlVMKv8lKlPFVPFGxSdUpopJZVJ5UvE3Hda6yGGtixzWusgPH1KZKiaVqWJSeVIxqTyp+ITKGxWfUJkqJpU3Kt6omFSeqPymw1oXOax1kcNaF/nhy1SmikllqphUJpWp4onKk4onFU9UpopJ5Y2KNyomlScVT1TeqPhNh7UucljrIoe1LvLDl1VMKm9UPFGZKqaKSeWJypOKqWJS+SaVqeJJxRsVb6g8qfimw1oXOax1kcNaF/nhQxVvVEwqU8WkMlVMKk8qJpWp4g2VJxVPVL5JZaqYVD5R8Tcd1rrIYa2LHNa6yA9fpvJE5YnKVDGpTBWTyqTyhsqTiknlEypvqEwVk8obFU9UporfdFjrIoe1LnJY6yI//LKKSeUNlScqb1R8QuUNlaliUnmi8kTljYpJZar4fzqsdZHDWhc5rHUR+4NfpPJGxROVqWJSmSomlaniDZUnFd+k8kbFpDJVTCpPKiaVJxWfOKx1kcNaFzmsdZEfvkxlqphUpopJ5RMVk8pU8YbKGypPKiaVqeJJxaTypGJSeVIxqTyp+KbDWhc5rHWRw1oXsT/4RSpvVDxR+UTFE5VvqphUnlS8ofJGxaQyVTxReVLxicNaFzmsdZHDWhf54ctUpoonKk9UnlS8oTJVTBWfUJlU3lB5UvFGxZOKSeX/6bDWRQ5rXeSw1kXsDz6g8qRiUpkqPqEyVUwqn6iYVKaKJypPKp6ofFPFE5UnFZPKVPGJw1oXOax1kcNaF/nhQxW/SeVJxTdVTCpTxaQyVUwVT1Q+UfFEZVJ5o2JS+U2HtS5yWOsih7Uu8sOXqUwVU8UTlaniiconKm5SMalMFZPKVDFVPFGZKt6o+KbDWhc5rHWRw1oX+eFDKlPFE5UnFZPKJyqeqEwVT1SeqDyp+E0qn1CZKqaKSWWq+MRhrYsc1rrIYa2L2B/8w1SeVDxR+UTFpPJGxRsqTyreUHlSMalMFd90WOsih7UucljrIj98SOVvqnhSMalMFVPFpDJVfKJiUplUfpPKVPGk4v/psNZFDmtd5LDWRX74sopvUvlExaTypGJSeVIxVXxTxaTyRsUbKk8qftNhrYsc1rrIYa2L/PDLVN6oeKNiUvmmiknlicpU8aRiUplU3lD5RMWk8kRlqvjEYa2LHNa6yGGti/zwj1OZKiaVJxWTylQxVTypeFLxiYpJ5UnFpDJVPKn4mw5rXeSw1kUOa13kh/+4ijcqJpWp4onKk4pJ5Q2VqeKJyhsqU8WTim86rHWRw1oXOax1kR9+WcVvqphU3qh4Q+U3VbyhMlW8ofIJlaniE4e1LnJY6yKHtS7yw5ep/E0qTyqeqDypmFSmikllqphUpopJ5UnFVPFGxaTyRGWqmCq+6bDWRQ5rXeSw1kXsD9a6xGGtixzWushhrYsc1rrIYa2LHNa6yGGtixzWushhrYsc1rrIYa2LHNa6yGGtixzWushhrYv8D90dFmgfq6akAAAAAElFTkSuQmCC' />

